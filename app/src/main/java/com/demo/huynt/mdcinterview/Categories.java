package com.demo.huynt.mdcinterview;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.util.ArrayList;
import java.util.List;


public class Categories extends AppCompatActivity {

    GridView gridCategories;
    ArrayList<Category> cate_data;
    ArrayList<String> cate_id;
    Toolbar toolbar;
    ProgressDialog progress;
    String cateId = new String("");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        progress=new ProgressDialog(this);
        progress.setMessage("Getting Template..");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);



        cate_data = new ArrayList<>();
        cate_id = new ArrayList<>();
        getCategoriesData();




        /*for(int i = 0;i<cate_name.length;i++){

            cate_data.add(new Category(cate_name[i]));
        }*/



    }

    public void getCategoriesData() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Template");
        query.findInBackground(new FindCallback<ParseObject>() {


            @Override
            public void done(final List<ParseObject> objects, ParseException e) {
                progress.dismiss();
                if (e == null) {
                    // If there are results, update the list of posts
                    // and notify the adapter
                    cate_data.clear();



                    final Bitmap[] bitmaps = new Bitmap[1];
                    bitmaps[0] = BitmapFactory.decodeResource(getResources(),
                            R.drawable.android);
                    for (int i = 0; i < objects.size(); i++) {
                        final int k = i;
                        cate_data.add(new Category(objects.get(k).getObjectId(),objects.get(k).getString("title"),bitmaps[0]));
                        ParseFile fileObject = objects.get(i).getParseFile("thumb");
                        fileObject.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {

                                bitmaps[0] = BitmapFactory.decodeByteArray(data,0,data.length);
                                cate_data.get(k).setImageID(bitmaps[0]);
                            }
                        });

                    }
                    gridCategories = (GridView) findViewById(R.id.gridView);
                    gridCategories.setAdapter(new CategoriesAdapter(Categories.this, cate_data));
                } else {
                    Log.d(getClass().getSimpleName(), "Load Data Error: " + e.getMessage());
                    Toast.makeText(Categories.this,"Không có dữ liệu tìm thấy",Toast.LENGTH_LONG).show();
                }

                gridCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.MyPreference),MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("Position", objects.get(position).getString("title"));
                        editor.commit();
                        cateId =objects.get(position).getObjectId();

                        Intent intent = new Intent(Categories.this,ResultActivity.class);
                        intent.putExtra("cateId",cateId);

                        startActivity(intent);
                    }
                });

            }
        });

    }
}



class CategoriesAdapter extends BaseAdapter{

    Context context;
    ArrayList<Category> cate_data;
    public CategoriesAdapter(Context context,ArrayList<Category> cate_data){

        this.context = context;
        this.cate_data = cate_data;
    }
    @Override
    public int getCount() {
        return cate_data.size();
    }

    @Override
    public Object getItem(int position) {
        return cate_data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(context);
            grid = inflater.inflate(R.layout.single_category, null);
            TextView textView = (TextView) grid.findViewById(R.id.txtItem_category_name);
            ImageView imageView = (ImageView)grid.findViewById(R.id.imgItem_category_pic);
            textView.setText(cate_data.get(position).cate_name);
            imageView.setImageBitmap(cate_data.get(position).cate_bitmap);
        } else {
            grid =  convertView;
        }


        return grid;
    }

}
/*class ViewHolder
{
    public ImageView imgCate_pic;
    public TextView txtCate_name;
    ViewHolder(View v){
        imgCate_pic = (ImageView) v.findViewById(R.id.imageView);
        txtCate_name = (TextView) v.findViewById(R.id.txtCate_name);
    }
}*/
class Category {

    public Bitmap cate_bitmap;
    public String cate_name;
    public String cat_ID;

    public String getCat_ID() {
        return cat_ID;
    }

    public void setCat_ID(String cat_ID) {
        this.cat_ID = cat_ID;
    }


    public Bitmap getImageID() {
        return cate_bitmap;
    }

    public void setImageID(Bitmap imageID) {
        this.cate_bitmap = imageID;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }

    public Category(String cat_ID, String cate_name, Bitmap bitmap) {

        this.cat_ID = cat_ID;
        this.cate_name = cate_name;
        this.cate_bitmap = bitmap;

    }
    public Category(){}
}
