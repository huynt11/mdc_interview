package com.demo.huynt.mdcinterview;



import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.view.menu.ActionMenuItemView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;



public class TopicDetailFragment extends Fragment implements View.OnClickListener{
    final static String ARG_PARAM1 = "inputType";
    final static String ARG_PARAM2 = "inputValue";
    final static String ARG_PARAM3 = "title";
    final static String ARG_PARAM4 = "description";

    String maram1 = "default1";
    String maram2 = "default2";
    String maram3 = "default3";
    String maram4 = "default4";

    TextView txtTitle, txtdescription;
    EditText txtResultStepper;


    ImageButton btnPLus,btnMinus;
    SharedPreferences sharedPreferences;
    EditText txtResultText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If activity recreated (such as from screen rotate), restore
        // the previous article selection set by onSaveInstanceState().
        // This is primarily necessary when in the two-pane layout.
        if (savedInstanceState != null) {
            maram1 = savedInstanceState.getString(ARG_PARAM1);
            maram2 = savedInstanceState.getString(ARG_PARAM2);
            maram3 = savedInstanceState.getString(ARG_PARAM3);
            maram4 = savedInstanceState.getString(ARG_PARAM4);
        }


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_topic_detail, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the article text.
        Bundle args = getArguments();
        if (args != null) {
            // Set article based on argument passed in
            maram1 = getArguments().getString(ARG_PARAM1);
            maram2 = getArguments().getString(ARG_PARAM2);
            maram3 = getArguments().getString(ARG_PARAM3);
            maram4 = getArguments().getString(ARG_PARAM4);

            updateArticleView(maram1,maram2,maram3,maram4);
        } else if (!maram3.equals("default")) {
            // Set article based on saved instance state defined during onCreateView
            updateArticleView(maram1,maram2,maram3,maram4);
        }
        ActionMenuItemView item = (ActionMenuItemView) getActivity().findViewById(R.id.finish_id);
        item.setVisibility(View.GONE);
        ActionMenuItemView item1 = (ActionMenuItemView) getActivity().findViewById(R.id.back);
        item1.setIcon(getResources().getDrawable(R.drawable.back));





    }



    public void updateArticleView(String inputType, String inputValue, String title, String description) {
        txtTitle = (TextView) getActivity().findViewById(R.id.txtTitle);
        txtTitle.setText(title);

        txtdescription = (TextView) getActivity().findViewById(R.id.txtdescription);
        txtdescription.setText(description);
        txtResultStepper = (EditText) getActivity().findViewById(R.id.txtResult_stepper);
        btnPLus = (ImageButton) getActivity().findViewById(R.id.btn_plus);
        btnMinus = (ImageButton) getActivity().findViewById(R.id.btn_minus);

        switch (inputType) {
            case "Stepper":
                break;
            case "Text":
                getViewforTextType();
                break;
            case "SingleChoice":
                getViewforSingleChoiceType(inputValue);
                break;
            default:
                break;
        }







    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current article selection in case we need to recreate the fragment
        outState.putString(ARG_PARAM1,maram1);
        outState.putString(ARG_PARAM2,maram2);
        outState.putString(ARG_PARAM3,maram3);
        outState.putString(ARG_PARAM4,maram4);

    }

    @Override
    public void onPause() {
        super.onPause();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.MyPreference),0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        ActionMenuItemView item = (ActionMenuItemView) getActivity().findViewById(R.id.finish_id);
        item.setVisibility(View.VISIBLE);
        ActionMenuItemView item1 = (ActionMenuItemView) getActivity().findViewById(R.id.back);
        item1.setIcon(getResources().getDrawable(R.drawable.refresh));



        String temp ;
        if(!txtResultStepper.getText().toString().equals("")){
            temp = txtResultStepper.getText().toString()+" /10";
        }
        else if(txtResultText != null){
            temp = txtResultText.getText().toString();
        }
        else {
            RadioGroup radioGroup = (RadioGroup) getActivity().findViewById(R.id.result_radio_group);
            temp = ((RadioButton)getActivity().findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
        }

        editor.putString(txtTitle.getText().toString(),temp);
        editor.apply();


    }

    private void getViewforTextType(){
        View v = getActivity().findViewById(R.id.conclusion_container);
        v.setVisibility(View.GONE);
        txtResultStepper.setText("");
        LinearLayout b = (LinearLayout) getActivity().findViewById(R.id.wrapper_conclusion_container);
        txtResultText = new EditText(getActivity());
        txtResultText.setId(R.id.txtResultText);
        txtResultText.setHint("Input Result..");
        b.addView(txtResultText);
        txtResultText = (EditText) getActivity().findViewById(R.id.txtResultText);
    }
    private void getViewforSingleChoiceType(String inputValue){
        View v = getActivity().findViewById(R.id.conclusion_container);
        v.setVisibility(View.GONE);
        txtResultStepper.setText("");
        RadioGroup radioGroup = (RadioGroup) getActivity().findViewById(R.id.result_radio_group);
        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
        final String[] parts = inputValue.split("\\|");

        for(String temp:parts){
            RadioButton rdbtn = new RadioButton(getActivity());
            rdbtn.setText(temp);
            final String test = temp;
            radioGroup.addView(rdbtn);
            rdbtn.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(getActivity(),"fap fap",Toast.LENGTH_SHORT).show();
        switch (v.getId()){
            case R.id.btn_plus:
                if(Integer.parseInt(txtResultStepper.getText().toString())==9){
                    txtResultStepper.setText("10");
                    btnPLus.setEnabled(false);
                }
                else {
                    txtResultStepper.setText(Integer.parseInt(txtResultStepper.getText().toString())+1);
                }
                break;
            case R.id.btn_minus:
                if(Integer.parseInt(txtResultStepper.getText().toString())==1){
                    txtResultStepper.setText("0");
                    btnMinus.setEnabled(false);
                }
                else {
                    txtResultStepper.setText(Integer.parseInt(txtResultStepper.getText().toString())-1);
                }
                break;
            default:
                break;
        }

    }


}