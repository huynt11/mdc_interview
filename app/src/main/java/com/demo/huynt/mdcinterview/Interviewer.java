package com.demo.huynt.mdcinterview;

/**
 * Created by HuyNT11 on 9/14/2016.
 */
public class Interviewer {
    private String inter_name;
    private String inter_email;

    public String getInter_name() {
        return inter_name;
    }

    public void setInter_name(String inter_name) {
        this.inter_name = inter_name;
    }

    public String getInter_email() {
        return inter_email;
    }

    public void setInter_email(String inter_email) {
        this.inter_email = inter_email;
    }

    public Interviewer(String inter_name, String inter_email) {
        this.inter_name = inter_name;
        this.inter_email = inter_email;
    }
}
