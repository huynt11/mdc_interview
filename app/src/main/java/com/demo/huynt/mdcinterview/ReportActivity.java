package com.demo.huynt.mdcinterview;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

public class ReportActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtCadicate, txtPosition;
    LinearLayout reportResultContainer;
    EditText etCurent, etExpected;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        createReportView();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_report,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String myPref = getResources().getString(R.string.MyPreference);
        sharedPref = getSharedPreferences(myPref, Context.MODE_PRIVATE);
        int numberInterviewers = sharedPref.getInt("interviewer_num",0);
        String[] interviewerList = new String[numberInterviewers];
        for (int i = 0; i < numberInterviewers; i++){
            interviewerList[i] = sharedPref.getString("email_"+i,"");
        }



        int res_id = item.getItemId();
        if (res_id == R.id.send) {



            Intent emailIntent = new Intent(Intent.ACTION_SEND);


            takeScreenshot(emailIntent);




            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, interviewerList);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Interview Report for "+txtCadicate.getText().toString()+" on "+txtPosition.getText().toString()+" Position");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");



            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                finish();
                Toast.makeText(this, "Sending Email..", Toast.LENGTH_SHORT).show();
            }
            catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
            }

        }

        return true;
    }

    private void createReportView() {
        txtCadicate = (TextView) findViewById(R.id.txtCandicate);
        txtPosition = (TextView) findViewById(R.id.txtPosition);
        reportResultContainer = (LinearLayout) findViewById(R.id.report_result_container);
        etCurent = (EditText) findViewById(R.id.txtCurrentSalary);
        etExpected = (EditText) findViewById(R.id.txtExpectedSalary);

        String myPref = getString(R.string.MyPreference);
        sharedPref = getSharedPreferences(myPref, Context.MODE_PRIVATE);
        txtCadicate.setText(sharedPref.getString(getString(R.string.candicate),"Default"));
        txtPosition.setText(sharedPref.getString("Position","Default"));


        int topicNumber = sharedPref.getInt("number_of_topics",0);



        String title, result;
        for (int i = 0; i < topicNumber; i++){
            TextView txtResult = new TextView(this);
            title = sharedPref.getString("topic"+i,"Chủ đề không tìm thấy");
            result = sharedPref.getString(title,"Kết quả không tìm thấy");
            txtResult.setText(title+" :\t\t"+result);
            txtResult.setTextSize(20);

            reportResultContainer.addView(txtResult);

        }
    }
    private void takeScreenshot(Intent emailIntent) {
        try {

            Date now = new Date();
            android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/Pictures/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            Uri path = Uri.fromFile(imageFile);
            emailIntent .putExtra(Intent.EXTRA_STREAM, path);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    /*private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }*/
}
