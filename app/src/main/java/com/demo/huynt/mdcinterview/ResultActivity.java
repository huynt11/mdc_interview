package com.demo.huynt.mdcinterview;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;


public class ResultActivity extends AppCompatActivity implements TopicFragment.OnFragmentInteractionListener {


    EditText txtResult;
    ImageButton btnPLus,btnMinus;
    String cateId;
    SharedPreferences sharedPreferences;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();

        Bundle b = getIntent().getExtras();
        cateId = b.getString("cateId");


        //Toast.makeText(this,cateId,Toast.LENGTH_LONG).show();

        TopicFragment topicFragment = new TopicFragment();
        Bundle args = new Bundle();
        args.putString(TopicFragment.ARG_PARAM1,cateId);
        topicFragment.setArguments(args);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(R.id.frag_container, topicFragment);
        ft.commit();





    }


    @Override
    public void onFragmentInteraction(String inputType,String inputValue, String title, String description) {


        TopicDetailFragment newFragment = new TopicDetailFragment();
        Bundle args = new Bundle();
        args.putString(TopicDetailFragment.ARG_PARAM1, inputType);
        args.putString(TopicDetailFragment.ARG_PARAM2, inputValue);
        args.putString(TopicDetailFragment.ARG_PARAM3, title);
        args.putString(TopicDetailFragment.ARG_PARAM4, description);
        newFragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag_container, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();




    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.interviewers_context_menu,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.finish_id){
            String myPref = getResources().getString(R.string.MyPreference);
            sharedPreferences = getSharedPreferences(myPref, Context.MODE_PRIVATE);
            int topicNumber = sharedPreferences.getInt("number_of_topics",0);
            ArrayList<String> topicsTitle = new ArrayList<>();
            ArrayList<String> topicsResult = new ArrayList<>();
            String result = "";
            for (int i = 0; i < topicNumber; i++){
                topicsTitle.add(sharedPreferences.getString("topic"+i,""));
                topicsResult.add(sharedPreferences.getString(topicsTitle.get(i).toString(),""));
                if(topicsResult.get(i).toString() == "") {
                    result = result + "- "+topicsTitle.get(i).toString()+",\n";
                }
            }
            if(result.equals("")) {
                Intent intent = new Intent(ResultActivity.this, ReportActivity.class);
                startActivity(intent);
            }
            else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Please input for the following topics: \n"+result)
                        .setTitle(R.string.dialog_title);
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        }
        if(item.getItemId()==R.id.back){
            TopicFragment topicFragment = new TopicFragment();
            Bundle args = new Bundle();
            args.putString(TopicFragment.ARG_PARAM1,cateId);
            topicFragment.setArguments(args);

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frag_container, topicFragment);
            ft.addToBackStack(null);
            ft.commit();
        }

        return true;
    }

    public void ChangeResultValue(View view) {
        txtResult = (EditText) findViewById(R.id.txtResult_stepper);
        btnPLus = (ImageButton) findViewById(R.id.btn_plus);
        btnMinus = (ImageButton) findViewById(R.id.btn_minus);
        int numberChose = Integer.parseInt(txtResult.getText().toString());


        switch (view.getId()){
            case R.id.btn_plus:

                if(numberChose== 9){
                    txtResult.setText(""+10);
                    btnPLus.setEnabled(false);
                }
                if(numberChose==0){
                    txtResult.setText("1");
                    btnMinus.setEnabled(true);
                }
                else {
                    txtResult.setText(""+(numberChose+1));
                }
                break;
            case R.id.btn_minus:
                if(numberChose == 1){
                    txtResult.setText("0");
                    btnMinus.setEnabled(false);
                }
                if(numberChose == 10 ){
                    txtResult.setText("9");
                    btnPLus.setEnabled(true);
                }
                else {
                    txtResult.setText(""+(numberChose-1));
                }
                break;
            default:
                Toast.makeText(view.getContext(),"default",Toast.LENGTH_SHORT).show();
                break;
        }

    }
}
