package com.demo.huynt.mdcinterview;

/**
 * Created by HuyNT11 on 9/19/2016.
 */
public class Topic {
    private String title;
    private String description;
    private String inputType;
    private String inputValues;

    public Topic() {
    }

    public Topic(String title, String description, String inputType, String inputValues) {
        this.title = title;
        this.description = description;
        this.inputType = inputType;
        this.inputValues = inputValues;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getInputValues() {
        return inputValues;
    }

    public void setInputValues(String inputValues) {
        this.inputValues = inputValues;
    }
}
