package com.demo.huynt.mdcinterview;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    ListView lvInterviewers;
    ArrayList<Interviewer> list_selected_interviewers = new ArrayList<>();


    ArrayAdapter<String> interview_adapter;
    EditText txtCandicate;
    SharedPreferences sharedPreferences;
    ProgressDialog progress;
    TextView txtDate;
    Calendar calendar;
    private int year, month, day;

    public ArrayList<Interviewer> interviewers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progress=new ProgressDialog(this);
        progress.setMessage("Getting Interviewers..");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();


        lvInterviewers = (ListView) findViewById(R.id.lvInterviewers);
        txtCandicate = (EditText) findViewById(R.id.txtCandicate);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        txtDate = (TextView) findViewById(R.id.txtDate);
        txtDate.setText(""+day +"/"+(month+1)+"/"+year);

        interviewers = new ArrayList<>();
        /*final ArrayList<String> listInterviewers = new ArrayList<String>();
        String[] values = getResources().getStringArray(R.array.interviewer);
        for(int i=0; i<values.length;i++){
            listInterviewers.add(values[i]);
        }

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_multiple_choice, listInterviewers);
        lvInterviewers.setAdapter(adapter);

        interviewers = new ArrayList<>();*/


        refreshInterviewersList();

        lvInterviewers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                list_selected_interviewers.clear();
                SparseBooleanArray checked = lvInterviewers.getCheckedItemPositions();
                for (int i = 0; i < checked.size(); i++) {
                    // Item position in adapter
                    if (checked.valueAt(i))
                        list_selected_interviewers.add(new Interviewer(interviewers.get(i).getInter_name(),interviewers.get(i).getInter_email()));
                }
            }
        });
    }

    private void refreshInterviewersList() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Interviewer");
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    // If there are results, update the list of posts
                    // and notify the adapter
                    interviewers.clear();
                    progress.dismiss();


                    for (int i=0; i<objects.size();i++) {

                        interviewers.add(new Interviewer(objects.get(i).getString("name"),objects.get(i).getString("email")));
                    }
                    ArrayList<String> temp_interviewer = new ArrayList<>();
                    for (int i=0; i<interviewers.size();i++){
                        temp_interviewer.add(interviewers.get(i).getInter_name());
                    }
                    interview_adapter = new ArrayAdapter<>(MainActivity.this,android.R.layout.simple_list_item_multiple_choice, temp_interviewer);
                    lvInterviewers.setAdapter(interview_adapter);
                    lvInterviewers.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                } else {
                    Log.d(getClass().getSimpleName(), "Load Data Error: " + e.getMessage());
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int res_id = item.getItemId();
        if(res_id == R.id.action_next){
            Intent intent = new Intent(MainActivity.this,
                    Categories.class);

            // Create a bundle object
            String[] outputStrArr = new String[list_selected_interviewers.size()];

            for (int i = 0; i < list_selected_interviewers.size(); i++) {
                outputStrArr[i] = list_selected_interviewers.get(i).getInter_name();
            }

            sharedPreferences = getSharedPreferences(getString(R.string.MyPreference),MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.putInt(getString(R.string.inter_num), outputStrArr.length);
            editor.putString(getString(R.string.candicate),txtCandicate.getText().toString());
            for(int i = 0;i < list_selected_interviewers.size();i++){
                editor.putString("inter_"+i,list_selected_interviewers.get(i).getInter_name());
                editor.putString("email_"+i,list_selected_interviewers.get(i).getInter_email());

            }
            editor.apply();
            String result = "";

            if(txtCandicate.getText().toString().replaceAll("\\s","").equals("")){
                result = result + getResources().getString(R.string.missing_candicate)+"\n";
            }
            if(outputStrArr.length == 0){
                result = result + getResources().getString(R.string.missing_interviewers)+"\n";
            }
            if( result.equals("")){
                startActivity(intent);
            }
            else{
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Please,  \n"+result +"Before continue..")
                        .setTitle(R.string.dialog_title);
                AlertDialog dialog = builder.create();
                dialog.show();
            }

        }


        return true;
    }

    public void OpenDatePicker(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void SetDateNow(View view) {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        txtDate = (TextView) findViewById(R.id.txtDate);
        txtDate.setText(""+day +"/"+(month+1)+"/"+year);
    }
}
