package com.demo.huynt.mdcinterview;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.ListFragment;

import android.support.v7.app.ActionBar;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import java.util.ArrayList;
import java.util.List;


public class TopicFragment extends ListFragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    static final String ARG_PARAM1 = "param1";


    // TODO: Rename and change types of parameters
    private String mParam1;


    private OnFragmentInteractionListener mListener;

    public ArrayList<Topic> topicList;

    private ArrayList<String> topic_arr = new ArrayList<>();
    private ArrayList<String> topic_inputType = new ArrayList<>();
    private ArrayList<String> topic_inputValue = new ArrayList<>();
    private ArrayList<String> topic_description = new ArrayList<>();

    private SharedPreferences sharedPreferences;
    private Toolbar toolbar;







    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }






        topicList = new ArrayList<>();

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.MyPreference),0);
        final SharedPreferences.Editor editor = sharedPreferences.edit();



        ParseQuery<ParseObject> query = ParseQuery.getQuery("Template");
        query.getInBackground(mParam1, new GetCallback<ParseObject>() {
            @Override
            public void done(final ParseObject object, com.parse.ParseException e) {
                if(e == null) {
                    ParseRelation<ParseObject> relation = object.getRelation("topics");
                    relation.getQuery().findInBackground(new FindCallback<ParseObject>() {

                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {
                            if(objects.size()>0) {
                                editor.putInt(getString(R.string.topic_number), objects.size());
                                for (int i = 0; i < objects.size(); i++) {

                                    topic_inputType.add(objects.get(i).getString("inputType"));
                                    topic_inputValue.add(objects.get(i).getString("inputValues"));
                                    topic_description.add(objects.get(i).getString("description"));
                                    topic_arr.add(objects.get(i).getString("title"));
                                    editor.putString("topic" + i, objects.get(i).getString("title"));
                                }

                            }
                            else {Toast.makeText(getActivity(),"Không có dữ liệu cho chủ đề này",Toast.LENGTH_LONG).show();}
                            setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, topic_arr));
                            editor.apply();
                        }
                    });
                }
                else{
                    Log.d(getClass().getSimpleName(), "Load Data Error: " + e.getMessage());
                }
            }


        });


        /*String[] values = getResources().getStringArray(R.array.topic);
        for(String temp:values){
            topic_inputType.add("Stepper");
            topic_inputValue.add("Beginner|Intermediate|Advance");
            topic_description.add(temp);
            topic_arr.add(temp);

        }*/



        // Create an array adapter for the list view, using the Ipsum headlines array


    }

    // TODO: Rename method, update argument and hook method into UI event

    public void onStart() {
        super.onStart();



        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the article text.
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //mListener.onFragmentInteraction(topicList.get(position).getInputType(),topicList.get(position).getInputValues(),topicList.get(position).getTitle(),topicList.get(position).getDescription());
        mListener.onFragmentInteraction(topic_inputType.get(position),topic_inputValue.get(position),topic_arr.get(position),topic_description.get(position));

    }


    /*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);





    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current article selection in case we need to recreate the fragment
        outState.putString(ARG_PARAM1, mParam1);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String inputType, String inputValue, String title, String description);
    }
}
